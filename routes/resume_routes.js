var express = require('express');
var router = express.Router();
var company_dal = require('../model/company_dal');
var address_dal = require('../model/address_dal');
var resume_dal = require('../model/resume_dal');
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');
var skill_dal = require('../model/skill_dal');


// View All companys
router.get('/all', function(req, res) {
    resume_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeViewAll', { 'result':result });
        }
    });

});

router.get('/selectuser', function(req, res) {
    account_dal.getAll(function(err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.render('resume/resumeAddSelectUser', {'result':result});
        }
    });
});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.company_id == null) {
        res.send('company_id is null');
    }
    else {
        company_dal.getById(req.query.company_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('company/companyViewById', {'result': result});
           }
        });
    }
});

// Return the add a new company form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    resume_dal.getById2(req.query.account_id, function(err, account) {
        school_dal.getById2(req.query.account_id, function (err, school) {
            skill_dal.getById2(req.query.account_id, function (err, skill) {
                company_dal.getById2(req.query.account_id, function (err, company) {
                    if (err) {
                        res.send(err);
                    }
                    else {

                        res.render('resume/resumeAdd', {
                            account: account,
                            school: school,
                            skill: skill,
                            company: company
                        });
                    }
                });
            });
        });
    });

});

// View the company for the given id
router.post('/insert', function(req, res){
    // simple validation
    if(req.body.resume_name == null) {
        res.send('Company Name must be provided.');
    }
    else if(req.body.school_id == null) {
        res.send('At least one school must be selected');
    }
    else if(req.body.skill_id == null) {
        res.send('At least one skill must be selected');
    }
    else if(req.body.company_id == null) {
        res.send('At least one company must be selected');
    }
    else if(req.body.account_id == null) {
        res.send('An account must be selected')
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        resume_dal.insert(req.body, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/resume/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('An account id is required');
    }
    else {
        resume_dal.edit(req.query.account_id, function(err, result){
            res.render('resume/resumeUpdate', {resume: result[0], school: result[1], skill: result[2], company: result[3]});
        });
    }

});

router.get('/edit2', function(req, res){
   if(req.query.company_id == null) {
       res.send('A company id is required');
   }
   else {
       company_dal.getById(req.query.company_id, function(err, company){
           address_dal.getAll(function(err, address) {
               res.render('company/companyUpdate', {company: company[0], address: address});
           });
       });
   }

});

router.get('/update', function(req, res) {
    company_dal.update(req.query, function(err, result){
       res.redirect(302, '/company/all');
    });
});

// Delete a company for the given company_id
router.get('/delete', function(req, res){
    if(req.query.company_id == null) {
        res.send('company_id is null');
    }
    else {
         company_dal.delete(req.query.company_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/company/all');
             }
         });
    }
});

module.exports = router;
