var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'SELECT * FROM account;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'select a.*, s.*, sk.*, c.* from account a ' +
    'left join account_school ash on ash.account_id = a.account_id ' +
    'left join school s on s.school_id = ash.school_id ' +
    'left join account_skill ask on ask.account_id = a.account_id ' +
    'left join skill sk on sk.skill_id = ask.skill_id ' +
    'left join account_company ac on ac.account_id = a.account_id ' +
    'left join company c on c.company_id = ac.company_id ' +
    'where a.account_id = ?; ';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};


exports.insert = function(params, callback) {

    // FIRST INSERT THE account
    var query = 'INSERT INTO account (email, first_name, last_name) VALUES (?)';

    var queryData = [params.email_name, params.first_name, params.last_name];

    connection.query(query, [queryData], function(err, result) {

        // THEN USE THE account_ID RETURNED AS insertId AND THE SELECTED school_IDs INTO account_school
        var account_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO account_school (account_id, school_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var accountSchoolData = [];
        if (params.school_id.constructor === Array) {
            for (var i = 0; i < params.school_id.length; i++) {
                accountSchoolData.push([account_id, params.school_id[i]]);
            }
        }
        else {
            accountSchoolData.push([account_id, params.school_id]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [accountSchoolData], function(err, result){
            //insert the skill
            var query = 'INSERT INTO account_skill (account_id, skill_id) VALUES ?';
       //     var account_id = result.insertId;
            var accountSkillData = [];
            if (params.skill_id.constructor === Array) {
                for (var i = 0; i < params.skill_id.length; i++) {
                    accountSkillData.push([account_id, params.skill_id[i]]);
                }
            }
            else {
                accountSkillData.push([account_id, params.skill_id]);
            }

            connection.query(query, [accountSkillData], function (err, result){
                // insert company for account
                var query = 'INSERT INTO account_company (account_id, company_id) VALUES ?';
              //  var account_id = account_id;
                var accountCompanyData = [];
                if (params.company_id.constructor === Array) {
                    for (var i = 0; i < params.company_id.length; i++) {
                        accountCompanyData.push([account_id, params.company_id[i]]);
                    }
                }
                else {
                    accountCompanyData.push([account_id, params.company_id]);
                }
               connection.query(query, [accountCompanyData], function(err, result){
                    callback(err, result);
                });
            });
        });
    });

};

exports.delete = function(account_id, callback) {
    var query = 'DELETE FROM account WHERE account_id = ?';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            companyAddressData.push([company_id, addressIdArray[i]]);
        }
    }
    else {
        companyAddressData.push([company_id, addressIdArray]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE company SET company_name = ? WHERE company_id = ?';
    var queryData = [params.company_name, params.company_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        companyAddressDeleteAll(params.company_id, function(err, result){

            if(params.address_id != null) {
                //insert company_address ids
                companyAddressInsert(params.company_id, params.address_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS company_getinfo;

     DELIMITER //
     CREATE PROCEDURE company_getinfo (_company_id int)
     BEGIN

     SELECT * FROM company WHERE company_id = _company_id;

     SELECT a.*, s.company_id FROM address a
     LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL company_getinfo (4);

 */

exports.edit = function(company_id, callback) {
    var query = 'CALL company_getinfo(?)';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};