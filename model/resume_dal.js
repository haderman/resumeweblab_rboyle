var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from company s
 join address a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'select a.*, resume_name from account a ' +
    'left join resume r on a.account_id = r.account_id ' +
    'where resume_name is not null; ';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(company_id, callback) {
    var query = 'SELECT c.*, a.street, a.zip_code FROM company c ' +
        'LEFT JOIN company_address ca on ca.company_id = c.company_id ' +
        'LEFT JOIN address a on a.address_id = ca.address_id ' +
        'WHERE c.company_id = ?';
    var queryData = [company_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.getById2 = function(account_id, callback) {
    var query = 'SELECT * FROM account ' +
        'WHERE account_id = ?;';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};


exports.insert = function(params, callback) {

    // FIRST INSERT THE Resume name to get id back
    var query = 'INSERT INTO resume (account_id, resume_name) VALUES (?);';

    var queryData = [params.account_id, params.resume_name];

    connection.query(query, [queryData], function(err, result) {

        // THEN USE THE COMPANY_ID RETURNED AS insertId AND THE SELECTED ADDRESS_IDs INTO COMPANY_ADDRESS
        var resume_id = result.insertId;

        // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
        var query = 'INSERT INTO resume_company (resume_id, company_id) VALUES ?';

        // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
        var companyResumeData = [];
        if (params.company_id.constructor === Array) {
            for (var i = 0; i < params.company_id.length; i++) {
                companyResumeData.push([resume_id, params.company_id[i]]);
            }
        }
        else {
            companyResumeData.push([resume_id, params.company_id]);
        }

        // NOTE THE EXTRA [] AROUND companyAddressData
        connection.query(query, [companyResumeData], function(err, result){


            // Insert the school for the resume
            var query = 'INSERT INTO resume_school (resume_id, school_id) VALUES ?';

            // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
            var schoolResumeData = [];
            if (params.school_id.constructor === Array) {
                for (var i = 0; i < params.school_id.length; i++) {
                    schoolResumeData.push([resume_id, params.school_id[i]]);
                }
            }
            else {
                schoolResumeData.push([resume_id, params.school_id]);
            }

            connection.query(query, [schoolResumeData], function(err, result) {


                // Insert the skill for the resume
                var query = 'INSERT INTO resume_skill (resume_id, skill_id) VALUES ?';

                // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
                var skillResumeData = [];
                if (params.skill_id.constructor === Array) {
                    for (var i = 0; i < params.skill_id.length; i++) {
                        skillResumeData.push([resume_id, params.skill_id[i]]);
                    }
                }
                else {
                    skillResumeData.push([resume_id, params.skill_id]);
                }

                connection.query(query, [skillResumeData], function(err, result) {
                    callback(err, result);
                });
            });
        });
    });

};

exports.delete = function(company_id, callback) {
    var query = 'DELETE FROM company WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

//declare the function so it can be used locally
var companyAddressInsert = function(company_id, addressIdArray, callback){
    // NOTE THAT THERE IS ONLY ONE QUESTION MARK IN VALUES ?
    var query = 'INSERT INTO company_address (company_id, address_id) VALUES ?';

    // TO BULK INSERT RECORDS WE CREATE A MULTIDIMENSIONAL ARRAY OF THE VALUES
    var companyAddressData = [];
    if (addressIdArray.constructor === Array) {
        for (var i = 0; i < addressIdArray.length; i++) {
            companyAddressData.push([company_id, addressIdArray[i]]);
        }
    }
    else {
        companyAddressData.push([company_id, addressIdArray]);
    }
    connection.query(query, [companyAddressData], function(err, result){
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressInsert = companyAddressInsert;

//declare the function so it can be used locally
var companyAddressDeleteAll = function(company_id, callback){
    var query = 'DELETE FROM company_address WHERE company_id = ?';
    var queryData = [company_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};
//export the same function so it can be used by external callers
module.exports.companyAddressDeleteAll = companyAddressDeleteAll;

exports.update = function(params, callback) {
    var query = 'UPDATE company SET company_name = ? WHERE company_id = ?';
    var queryData = [params.company_name, params.company_id];

    connection.query(query, queryData, function(err, result) {
        //delete company_address entries for this company
        companyAddressDeleteAll(params.company_id, function(err, result){

            if(params.address_id != null) {
                //insert company_address ids
                companyAddressInsert(params.company_id, params.address_id, function(err, result){
                    callback(err, result);
                });}
            else {
                callback(err, result);
            }
        });

    });
};

/*  Stored procedure used in this example
     DROP PROCEDURE IF EXISTS company_getinfo;

     DELIMITER //
     CREATE PROCEDURE company_getinfo (_company_id int)
     BEGIN

     SELECT * FROM company WHERE company_id = _company_id;

     SELECT a.*, s.company_id FROM address a
     LEFT JOIN company_address s on s.address_id = a.address_id AND company_id = _company_id;

     END //
     DELIMITER ;

     # Call the Stored Procedure
     CALL company_getinfo (4);

 */

exports.edit = function(account_id, callback) {
    var query = 'CALL resume_getinfo(?)';
    var queryData = [account_id];

    connection.query(query, queryData, function(err, result) {
            callback(err, result);
        });
};